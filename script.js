const blocks = document.getElementsByClassName("block");
for (let block of blocks) {
    for (let child of block.children) {
        if (child.className === "headline") {
            child.textContent = "архитектура";
        }
        else if (child.className === "description") {
            child.textContent = "з0чем";
        }
        else if (child.className === "image") {
            child.src = "https://source.unsplash.com/320x420/?architecture";
        }
    }
}